---
layout: product
title: Affiche d'appel à la grève - Mesures minimales
subtitle: Publiée le 17 mars 2020 sur paris-luttes.info
image: /greve-des-loyers/images/uploads/covid_19_2.jpg
product_code: AFFI003
---
Il s'agit d'une affiche appelant à la grève des loyers. Elle est écrite en blanc (code couleur `#FFFFFF`) sur fond noir (code couleur `#000000`).

Un masque de catch est placé en haut à droite, suivi d'un long texte.

Le texte dit :

> COVID-19
> MESURES MINIMALES 
> liste évolutive mise à jour 16 mars 2020
> 
> - Organisation de structures d’entraide (courses, veille, soins) pour les personnes fragiles et isolées.
> 
> - Refonte totale du système de santé (augmentation des postes, lits, hôpitaux, gratuité universelle).
> 
> - Suppression des jours de carence pour tou.tes.
> 
> - Revenu garanti au SMIC mensuel a minima pour tou.tes.
> 
> - Fermeture des lieux de production non essentiels à la résolution de la crise sanitaire.
> 
> - Droit de retrait pour tou.tes (hors secteurs indispensables) avec indemnisation totale.
> 
> - Aide financière exceptionnelle aux travailleur.euses indépendant.es et précaires.
> 
> - Prolongation des droits au chômage et pour les intermittent.es, au prorata des jours de confinement.
> 
> - Interdiction des licenciements et des expulsions de domicile.
> 
> - Suspension des loyers, des factures d’énergie et des crédits pour les personnes physiques et morales.
> 
> - Réquisition des fabricant.es de matériel médical et des cliniques privées.
> 
> - Réquisition des logements vacants pour les personnes sans-abri, victimes de violences masculines, exilé.es.
> 
> - Libération des personnes en détention provisoire, des prisonnier.es et des réfugié.es.
> 
> - Annulation des obligations de quitter le territoire (oqtf).
> 
> - Abrogation définitive des lois sécuritaires et antisociales des 40 dernières années.
> 
> NOTA BENE : Les conséquences de cette crise sanitaire étant dues aux politiques libérales des 40 dernières années, soyons déterminé·es à nous battre de toutes nos forces pour que rien ne retourne à la normale une fois qu’on en aura fini avec ce virus.

