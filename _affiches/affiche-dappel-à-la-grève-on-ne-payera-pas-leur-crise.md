---
layout: product
title: Affiche d'appel à la grève - On ne payera pas leur crise
subtitle: Publiée le 21 mars 2020
image: /greve-des-loyers/images/uploads/affiche.png
product_code: AFFI002
---
Il s'agit d'une affiche appelant à la grève des loyers. Elle est écrite en rouge (code couleur `#E80413`) sur fond crème (code couleur `#FAF4B8`).

Une image en haut à gauche représente une ville au dessus de laquelle se trouve un très gros soleil, qui ressemble à un virus. On voit l'ombre d'une personne qui semble très inquiète et qui se tient la tête sur le mur d'un des bâtiments.

Le texte dit :

> ON NE PAIERA PAS LEUR CRISE
>
> #COVID2019
> #COVID_GreveDesLoyers
> #COVID_DontPayYourBills
> #CONFINEMENT
> 
> IMMUNITÉ COLLECTIVE
> 
> Campagne pour la grève des loyers
> 
> NOUS EXIGEONS :
> 
> 1- Santé gratuite
> Tests, protections, traitement et soin pour tou.te.s
> 
> 2- Arrêt du travail
> Suspension du travail obligatoire y compris le télétravail
> 
> 3-  Ni paiement ni dettes
> Suspension de tous les loyers, hypothèques, charges, remboursements de prêts, et saisies de biens
> 
> 4- Libération des prisonnier.e.s
> 
> 5- Logements pour tou.te.s
> Arrêt des explusions et ouverture des logements vides.
> 
> POUR TOU.TE.S CELL-EUX QUI EN ONT BESOIN