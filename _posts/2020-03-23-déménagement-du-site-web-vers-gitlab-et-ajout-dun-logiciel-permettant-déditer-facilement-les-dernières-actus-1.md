---
layout: post
title: >-
  Déménagement du site web vers GitLab et ajout d'un logiciel permettant
  d'éditer facilement les dernières actus
date: 2020-03-23T17:31:35.644Z
image: /greve-des-loyers/images/uploads/working-hard-work-man.jpg
published: true
---
Petite news pour vous expliquer ce que nous avons fait cet après-midi !

# Déménagement du site web

Pour des raisons de compatibilité avec un logiciel vous permettant de facilement rajouter du contenu au site, nous avons déménagé le dépôt du projet. Son URL a donc également changé : <https://darckcrystale.gitlab.io/greve-des-loyers>

# Installation de Netlify pour vous permettre de rajouter du contenu

Nous avons mis en place Netlify, qui vous permettra de facilement rajouter du contenu au site : news et ressources notamment.

Pour accéder à l'interface, connectez-vous à l'administration : <https://darckcrystale.gitlab.io/greve-des-loyers/admin>

Une fois connecté.e, vous pourrez créer de nouveaux billets qui seront ajoutés aux dernières actualités, éditer les billets existant ; et également rajouter et éditer des ressources !