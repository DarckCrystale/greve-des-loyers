---
layout: post
title: Réunion grève des loyers 26 mars 2020 à 17h
date: 2020-03-25T17:22:05.623Z
image: /greve-des-loyers/images/uploads/photo_2020-03-25_16-32-20.jpg
published: true
---
Prochaine réunion Mumble jeudi 26 mars 2020 à 17h

## Tutoriel Mumble

> Pour découvrir comment utiliser Mumble, un tutoriel est mis à votre disposition ici : https://docs.framasoft.org/fr/jitsimeet/mumble.html

## Informations de connexion

Adresse du Mumble : mumble.framatalk.org Channel du Mumble : Organisation grève des loyers

## Informations générales

**Date de la réunion :** jeudi 26 mars 2020

**Heure de la réunion :** 17h

**Animateurice de la réunion :** Hippolyte

**Durée de la réunion :** 1h30 (+ ou - 30 minutes)

## Ordre du jour

* tour de présentation si on n'est pas plus d'une quinzaine de personnes
* petit point situation politique et utilité de l'action
* répartition des rôles (prise de notes/compte rendu, gestion du tems, gestion des tours de parole)
* retour des groupes de travail : ce qui a été fait, ce qui reste à faire, besoins éventuels
* lancement de la campagne de communication le plus rapidement possible
* répartition des tâches
* questions subsidiaires éventuelles

N'hésitez pas à venir, même si c'est juste pour écouter !